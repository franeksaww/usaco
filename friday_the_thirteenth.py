weekday = 1
year = 1900
months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
data = [0 for i in range(7)]
f = open('friday.in', 'r')
for i in range(int(f.readline().rstrip())):
    months[1] = 28
    if (year % 4 == 0 and year % 100 != 0) or\
            (year % 100 == 0 and year % 400 == 0):
        months[1] = 29
    for l in range(12):
        data[(weekday + 13) % 7] += 1
        weekday = (weekday + months[l]) % 7
    year += 1
results = [str(d) for d in data]
f = open('friday.out', 'w+')
f.write(' '.join(results) + '\n')
