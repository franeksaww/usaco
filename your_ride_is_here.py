import string


def calculate(name):
    result = 0
    for letter in name:
        if result == 0:
            result = (string.ascii_lowercase.index(letter) + 1)
        else:
            result *= (string.ascii_lowercase.index(letter) + 1)
    return result


file = open('ride.in', 'r')
e = file.read().split('\n')
if calculate(e[0].lower()) % 47 == calculate(e[1].lower()) % 47:
    end = 'GO'
else:
    end = 'STAY'

file = open('ride.out', 'w')
file.write(end + '\n')
