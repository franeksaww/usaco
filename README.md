### Challanges from USACO

1: Ride
- your_ride_is_here.py
- https://train.usaco.org/usacoprob2?a=5M1od35KJsq&S=ride

2: Greedy Gift Givers
- greedy_gift_givers.py
- https://train.usaco.org/usacoprob2?a=T7LNipDVDl1&S=gift1

3: Friday the Thirteenth
- friday_the_thirteenth.py
- https://train.usaco.org/usacoprob2?a=T7LNipDVDl1&S=friday

4: Broken Necklace
- broken_necklace.py
- https://train.usaco.org/usacoprob2?a=CGHvE3WHfFW&S=beads
