with open('beads.in', 'r') as f:
    beads_count = int(f.readline())
    beads = f.readline()[:-1]


def is_white(s):
    return not ('r' in s and 'b' in s)


beads = beads*3
max_value = 0
for p in range(beads_count, beads_count * 2):
    i = p-1
    left = []
    while i > 0:
        if is_white(left + [beads[i]]):
            left.append(beads[i])
            i -= 1
        else:
            break
    i = p
    right = []
    while i < 3*beads_count - 1:
        if is_white(right + [beads[i]]):
            right.append(beads[i])
            i += 1
        else:
            break
    result = len(left) + len(right)
    if result >= beads_count:
        max_value = beads_count
        break
    elif result > max_value:
        max_value = result

with open('beads.out', 'w') as fileout:
    fileout.write(str(max_value) + '\n')
