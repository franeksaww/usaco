f = open('gift1.in', 'r')
group = f.readline()
friends = {}
for i in range(int(group)):
    friends[f'{f.readline().rstrip()}'] = 0
for l in range(int(group)):
    giver = f.readline().rstrip()
    amount, to = f.readline().rstrip().split(' ')
    if int(amount) != 0 and int(to) != 0:
        rest = int(amount) % int(to)
        to_give = int(amount) - rest
        friends[f'{giver}'] -= to_give
        for k in range(0, int(to)):
            friends[f'{f.readline().rstrip()}'] += to_give / int(to)
    elif int(amount) != 0 and int(to) == 0:
        friends[f'{giver}'] += int(amount)
    else:
        for g in range(int(to)):
            f.readline()
f = open('gift1.out', 'w+')
for i in list(friends.keys()):
    f.write(i + ' ' + str(int(friends[f'{i}'])) + '\n')
